#!/usr/bin/env python

import sys
import os
from librabbitmq import Connection

connection = Connection(host=os.getenv('RABBITMQ_HOST', 'localhost'), userid=os.getenv('RABBITMQ_USERID', 'guest'), password=os.getenv('RABBITMQ_PASSWORD', 'guest'), virtual_host=os.getenv('RABBITMQ_VHOST', '/'))

channel = connection.channel()
channel.queue_bind(os.getenv('RABBITMQ_QUEUE', 'dump1090'), os.getenv('RABBITMQ_EXCHANGE', 'dump1090'), os.getenv('RABBITMQ_ROUTING_KEY', 'dump1090'))

while True:
  line = sys.stdin.readline()
  if not line: break #EOF
  channel.basic_publish(line.strip(), os.getenv('RABBITMQ_EXCHANGE', 'dump1090'), os.getenv('RABBITMQ_ROUTING_KEY', 'dump1090'), delivery_mode = 2)
