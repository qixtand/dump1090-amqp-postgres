#!/bin/bash

###
# Setup a RabbitMQ server
###
sudo sh -c 'echo "deb http://www.rabbitmq.com/debian/ testing main" > /etc/apt/sources.list.d/rabbitmq.list'
wget --quiet -O - https://www.rabbitmq.com/rabbitmq-signing-key-public.asc | sudo apt-key add -
sudo apt-get update

sudo apt-get install rabbitmq-server -y

sudo sh -c 'echo "ulimit -n 1024" >> /etc/default/rabbitmq-server'
sudo service rabbitmq-server restart

# Make the management console available on port 15672
sudo rabbitmq-plugins enable rabbitmq_management

# Install rabbitmqadmin
sudo wget http://localhost:15672/cli/rabbitmqadmin -O /usr/local/bin/rabbitmqadmin
sudo chmod 755 /usr/local/bin/rabbitmqadmin
sudo sh -c 'rabbitmqadmin --bash-completion > /etc/bash_completion.d/rabbitmqadmin'

# Add a user for remote access and promote to admin
sudo rabbitmqctl add_user admin password
sudo rabbitmqctl set_user_tags admin administrator

# Prepopulate the rabbitmq.config; it doesn't exist by default
sudo sh -c 'echo "[]." > /etc/rabbitmq/rabbitmq.config'

# Declare a queue
rabbitmqadmin declare queue name=dump1090 durable=true


###
# Setup for the push and poll programs
###
#sudo apt-get install python-dev python-pip python-psycopg2 -y
#sudo pip install librabbitmq
